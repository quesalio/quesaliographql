/* ---------------------------------------------------------------------------------
   * @ description : This is the server configration file.
---------------------------------------------------------------------------------- */

import './utilities/global';
import Hapi from 'hapi';
import config from 'config';
import plugins from './plugins';
import logger from './utilities/logger';
import { failActionJoi } from './utilities/rest';
import fs from 'fs';

const app = config.get('app');

export default async () => {
  const tls = {
    key: fs.readFileSync(
      '/home/quesalio/ssl/keys/bf9dd_eb0df_b0c760e83721ee09198a988c499eed38.key'
    ),
    cert: fs.readFileSync(
      '/home/quesalio/ssl/certs/quesalio_com_bf9dd_eb0df_1550719247_5e38a12d69b61dd1463b65a734db0ffb.crt'
    )
  };
  const server = new Hapi.Server({
    host: app.host,
    port: app.port,
    tls: tls,
    routes: {
      cors: {
        origin: ['*'],
        additionalHeaders: ['authorization'],
        additionalExposedHeaders: ['authorization']
        //additionalHeaders: ['*'],
        //additionalExposedHeaders: ['*']
      },
      validate: {
        failAction: failActionJoi
      }
    }
  });

  const server1 = new Hapi.Server({
    host: 'production.quesalio.com',
    port: 4007,
    tls: tls,
    routes: {
      cors: {
        origin: ['*'],
        additionalHeaders: ['authorization'],
        additionalExposedHeaders: ['authorization']
        //additionalHeaders: ['*'],
        //additionalExposedHeaders: ['*']
      },
      validate: {
        failAction: failActionJoi
      }
    }
  });

  await server.register(plugins);
  await server1.register(plugins);

  try {
    await server.start();
    await server1.start();
  } catch (err) {
    logger.info(`Error while starting server: ${err.message}`);
  }

  logger.info(`+++ Server running at: ${server.info.uri}`);
};
